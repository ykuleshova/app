# EPAM Diploma :: Backend

Backend part of the project

## Installation

To install backend, execute the following commands from the root of you web directory:

    $ git clone https://gitlab.com/ykuleshova/app.git ./tmp
    $ shopt -s globopt && cp ./backend/* ./
    $ rm -rf ./tmp

In case of keeping github syncronization you can clone repository to desired path and point webroot of you webserver (PHP-FPM) on backend folder.
    
## Configuration

1. Copy and edit evironment file

   >$ cp .env.example .env


2. Install Composer

    Follow official [documentation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos) or simple copy&paste next (check you have php-cli properly installed in your system):
    
    >$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    $ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
   
    check composer installed
    >$ composer --version
   
3. Install reuirements
    >$ composer install
   

## Usage

Try to access URL `http://<your_web_server>/`. 

You should see JSON resopnse, something like:

>{ 'status': 'ok', data: {...} }


## Verion history

11.12.2021 v1.0
 - Main functionality

### Legal information
This project was created as a EPAM diploma task by Iuliia Kuleshova.
