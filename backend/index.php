<?php

require "vendor/autoload.php";

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpClient\CurlHttpClient;

class App
{
    protected $httpClient;
    protected mysqli $db;

    public static function init()
    {
        $app = new static();
        $app->db->query('CREATE TABLE IF NOT EXISTS data ( date DATE NOT NULL, tmin FLOAT(4) NOT NULL, tmax FLOAT(4) NOT NULL, humidity FLOAT(4) NOT NULL)');
    }

    /***
     * Application constructor
     */
    public function __construct()
    {
        $env = new Dotenv();
        $env->load(__DIR__ . '/.env');

        $this->httpClient = new CurlHttpClient();
        $this->db = mysqli_connect($_ENV['db_host'], $_ENV['db_user'], $_ENV['db_pass'], $_ENV['db_name'], $_ENV['db_port']) or die('DB connection error.' . mysqli_error());
    }

    /***
     * Retrieve data from remote API
     *
     * @param string $date
     * @return array|false
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getRemoteData($date)
    {
        list($year, $month, $day) = explode('-', $date);
        $apiResource = 'https://www.metaweather.com/api/location/2123260/' . $year . '/' . $month . '/' . $day . '/';
        $response = $this->httpClient->request('GET', $apiResource);
        $contentArr = json_decode($response->getContent(), true);

        $tmin = 100;
        $tmax = $humidity = -100;
        foreach ($contentArr as $row) {   
    	    if (strpos($row['created'], $date) === false) continue;
            $tmin = $row['the_temp'] < $tmin ? $row['the_temp'] : $tmin;
            $tmax = $row['the_temp'] > $tmax ? $row['the_temp'] : $tmax;
            $humidity = $humidity == -100 ? $row['humidity'] : ($humidity + $row['humidity']) / 2;
        }
        return $tmin == 100 ? false : ['tmin' => $tmin, 'tmax' => $tmax, 'humidity' => $humidity];
    }

    /***
     * Retrieve data from database otherwise returns false
     *
     * @param $date
     * @return array|false|null
     */
    public function getFromDB($date)
    {
        $query = 'SELECT * FROM data WHERE date = ?';
        $result = [];
        if ($stmt = $this->db->prepare($query)) {
            $stmt->bind_param("s", $date);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_assoc();
        }
        return count($result) > 0 ? $result : false;
    }

    /***
     * Store data in database
     *
     * @param $date
     * @param $data
     */
    public function storeToDB($date, $data)
    {
        $query = 'INSERT INTO data (date, tmin, tmax, humidity) VALUES (?, ?, ?, ?)';
        if ($stmt = $this->db->prepare($query)) {
            $stmt->bind_param('sddd', $date, $data['tmin'], $data['tmax'], $data['humidity']);
            $stmt->execute();
        }
    }

    /***
     * Retrieve data from local database or remote API if fails local.
     *
     * @param null $date
     * @return array|false|null
     */
    public function getLocalData($date = null)
    {
        if ($data = $this->getFromDB($date))
            return $data;
        $data = $this->getRemoteData($date);
        if ($data !== false) {
            $this->storeToDB($date, $data);
        }
        return $data;
    }

    /***
     * Retrieve data from somewhere
     *
     * @return array
     */
    public function getData()
    {

        $yesterday = strtotime('yesterday');
        $yesterdayLastYear = date('Y-m-d', strtotime('1 year ago', $yesterday));
        $yesterday = date('Y-m-d', $yesterday);

        $yesterdayData = $this->getLocalData($yesterday);
        $yesterdayLastYearData = $this->getLocalData($yesterdayLastYear);

        return [
            'yesterday' => array_merge($yesterdayData, ['date' => $yesterday]),
            'yesterdayLastYear' => array_merge($yesterdayLastYearData, ['date' => $yesterdayLastYear])
        ];
    }
}

if (!isset($_SERVER["DOCUMENT_ROOT"]))
    return;

try {
    $app = new App();
    $data = $app->getData();
    // just for fun - I like ghosts :)
    sleep(1);
    echo json_encode(['status' => 'ok', 'data' => $data]);
} catch (Exception $e) {
    echo json_encode(['status' => 'error']);
}
