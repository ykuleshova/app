# Diploma app

Diploma web application

This project collects some meteorogical information from www.metaweather.com and stores in local database. User can view information for yesterday and the same day one year ago.

The project consists of two parts: frontend and backand (database is a separate part and should be installed before).
 
For more informtion about each part see related Readme.md in folders [Frontend](frontend/README.md) / [Backend](backend/README.md).

### Legal information
This project was created as a EPAM diploma task by Iuliia Kuleshova.

