# EPAM Diploma :: Frontend

Frontend part of the project

## Installation

To install frontend, execute the following commands from the root of you web directory:

    $ git clone https://gitlab.com/ykuleshova/app.git ./tmp
    $ shopt -s globopt && cp ./tmp/frontend/* ./
    $ rm -rf ./tmp

In case of keeping github syncronization you can clone repository to desired path and point webroot of you webserver on frontend folder.

## Configuration

Setup your web server to proxy requests for backend.
For example (Nginx):
```
upstream diploma_backend {
   server <ip_address>;
}

location /api {
   proxy_pass http://diploma_backend;
}
```

## Usage

Try to access URL `http://<your_web_server>/`. 


## Verion history

11.12.2021 v1.0
 - Main functionality


### Legal information
This project was created as a EPAM diploma task by Iuliia Kuleshova.
